<DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Berlatih PHP</title>
    </head>
    <body>
        <?php
            require_once ('Animal.php');
            require_once ('Ape.php');
            require_once ('Frog.php');

            $sheep = new Animal("shaun");

            echo "Name: " . $sheep->name . "<br>";
            echo "<br>";
            echo "Legs: " . $sheep->legs . "<br>";
            echo "<br>";
            echo "Cold Blooded: " . $sheep->cold_blooded . "<br>";
            echo "<br>";
            echo "================================================ <br>";
            echo "<br>";

            $sungokong = new Ape("kera sakti");
            $sungokong->yell();
            echo "<br>";
            echo "<br>";
            echo "Name: " . $sungokong->name . "<br>";
            echo "<br>";
            echo "Legs: " . $sungokong->legs . "<br>";
            echo "<br>";
            echo $sungokong->cold_blooded . "<br>";
            echo "<br>";
            echo "================================================ <br>";
            echo "<br>";

            $kodok = new Frog("buduk");
            $kodok->jump();
            echo "<br>";
            echo "<br>";
            echo "Name: " . $kodok->name . "<br>";
            echo "<br>";
            echo "Legs: " . $kodok->legs . "<br>";
            echo "<br>";
            echo "Cold Blooded: " . $kodok->cold_blooded . "<br>";
            echo "<br>";
        ?>
    </body>
</html>